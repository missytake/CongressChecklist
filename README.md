# CongressChecklist


## ⏰ Things to do before traveling

- [ ] Stock you pantry/freezer with some food that keeps so you have something to eat when you get home (that doesn't rely on shops/restaurants being open).


## 🎒 Things to pack


### Documents
- [x] Congress tickets (printed or digital. iOS passbook/wallet, PDF)
  - [ ] have them ready before your line up at the cash desk!
- [x] Passport or other kind of photo ID (Personalausweis)
- [x] An [organ donor card][21] (We really hope you won't need it, but just in the case: you might save a life!)
- [x] Health insurance card


### Payment stuff
When using ATMs/Cash Machines/Bankomaten/Geldautomaten give them a good wiggle before inserting your card. During 32C3 a Skimmer device had been found at an ATM at Bahnhof Dammtor. This is just a reminder, you should do that every time you use your card.

- [x] Some cash (€uros, or other local currency)
- [x] Maestro card (Bankomatkarte, EC-Karte)
- [x] Coins (€uros, for automated vending machines, like public transport tickets)


### Mobility
- [x] An app to show local public transport times. Not all cities are available in Google/Apple/You-name-it Maps applications.
- [x] A [towel][49]


### Personal hygiene
- [x] Tooth brush (plus charger if it is an electrical one)
- [x] Toothpaste
- [x] Shower gel
- [x] Condoms, dental dams and other protective gear (Make sure the condoms are not beyond their usage date! (Yes, it really happens…))
- [x] Lubricant (silicone or water based, to be compatible with condoms)
- [x] Handkerchiefs


### Cosmetics
- [x] Make up
- [x] Nail polish, also handy to secure screws on your laptop and have a tamper indicator, especially when using one with glitter.
- [x] Nail polish remover
- [x] Razor (with spare blades / plus cable/charger if it is an electrical one)
- [x] Shaving soap
- [x] Lipstick/lipgloss
- [x] Skin care


### Clothing
Come as you are, as a friend.

- [x] At least ONE clean shirt for each day you stay, plus two for traveling to and from congress (nerdy print is always welcome)
- [x] At least ONE set of clean underwear for each day you stay, plus two for traveling to and from congress (Doesn't necessarily need to be in matching pairs, but that is up to you.)
- [x] At least ONE pair of clean socks/stockings/tights/overknees for each day you stay, plus two for traveling to and from congress (again, matching pairs optional)
- [x] Enough pants/skirts/trousers/dresses/etc. so you can wear clean clothes every day you attend congress (even if somebody accidentally (or intentionally) spills a beverage all over you!)
- [x] Hoodies, pullovers, zippers, vests (it's cold outside!)
- [x] Jacket, coat (wind proof, at least somewhat water resistant)
- [x] Comfortable shoes (You'll cover a lot of distance-units during your stay)
- [x] Gloves
- [x] Working Gloves
- [x] Scarves
- [x] High Visibility Vest (to be visible during Angel Work)


### Gear
Label ALL your gear in a consistent manner. (Twitter handle and DECT numbers have proven to be working well.)
In case something gets lost it can find its way back to you.


#### Power and Chargers

- [x] Charger for laptop
- [x] 3x to 10x multi-plug power extension (230V AC @50Hz) with [Schuko Plug][6] (and optional switch, hotel rooms usually suffer from severe lack of outlets)
- [x] Chargers for all those phones, or at least matching cables if you can charge over USB.
- [x] A portable battery pack (aka powerbank) so you can charge on the go. (And the necessary cable to recharge that battery pack overnight. A USB-A to micro USB cable in most cases, maybe already a USB-C cable as well.)


#### Laptop and Devices
- [x] Laptop (with a working battery)
	- [ ] Spare Laptop
- [x] USB Tails drives
- [x] Mass storage devices (bring some empty ones to fill with data and/or bring your own data and share it!)
- [x] An actual book (for those offline blackout moments), see [B.O.O.K.][32]


#### Wireless and HF
- [ ] WiFi router/firewall (to connect to the hotel Wifi/network)


#### Cables and Adapters (Dongles)
You'll forget at least one anyway, so don't worry. Bring a spare to help out friends!


##### Networking (wired and wireless)
- [x] Ethernet cords (Cat5e) of multiple lengths (bring a spare to tether a friend)


##### Audio
- [x] Headphones
- [x] Bluetooth Box
- [-] Aux-Kabel


#### Mobile and Communications
- [ ] Phones (All of them)
- [x] Smartphones
- [ ] SIM cards 


#### Other
- [x] Headlamp
    - [ ] AAA Batteries
- [ ] Headphones (Closed type) (so you don't have to talk/listen to other carbon units while traveling)
- [ ] Soldering iron
    - [ ] solder
    - [ ] solder pump
    - [ ] flux
- [ ] Duct tape or gaffer tape (vital)
- [ ] Easy snacks such as nuts or granola bars (Events like these are often hectic and sometimes it's easy to forget to eat. If you need food to take medications, make sure to always have snacks with you)



### Schedule
- [ ] Have a look at the Self-Organized Sessions
- [ ] Note down the sessions I help organizing


### PowerOn/BIOS/EFI Password
- [ ] Make sure you do have set a BIOS or EFI Password to prevent booting from a live CD to attack your system.


### Phones
It is not recommended to connect your smartphone to the congress wifi -- [ ] if you still can't resist, here are a few things to consider:

- [ ] Make sure that it's **not a 802.11b** device, as it would slow down the complete wifi network.
- [ ] Disable all synchronization services, as they might be using plaintext.
- [ ] Disable all auto-discovery services (network music players, remote control apps, etc …).
- [ ] Make sure there are no known exploits for your device (not that unknown-to-you exploits wouldn't be a possible risk as well…).
- [ ] Turn off push notifications.
- [ ] Update your apps, especially your browser.
- [ ] Use a VPN tunnel if possible.
- [ ] Use a Firewall and only allow the (few) services you trust.
	- [ ] [DroidWall][22] for Android (root required)
- [ ] If you don't have a VPN tunnel available, you can use [OnionBrowser][7] (iOS) or [Orbot][orbot] (Android) to surf via Tor (which may be its own kind of risk).
- [ ] Set your GSM network selection to **manual** or you might end up connected to a rogue GSM network.

**If you are unsure** if there are still some services transmitting plaintext data, **do not connect**.


